﻿namespace Calculator
{
    /// <summary>
    /// Базовый класс fluent-калькулятора.
    /// </summary>
    public abstract class FluentCalculatorBase
    {
        private protected float _value;
        public abstract FluentCalculatorBase one { get; }
        public abstract FluentCalculatorBase two { get; }
        public abstract FluentCalculatorBase three { get; }
        public abstract FluentCalculatorBase four { get; }
        public abstract FluentCalculatorBase five { get; }
        public abstract FluentCalculatorBase six { get; }
        public abstract FluentCalculatorBase seven { get; }
        public abstract FluentCalculatorBase eight { get; }
        public abstract FluentCalculatorBase nine { get; }
        public abstract FluentCalculatorBase ten { get; }
        public abstract FluentCalculatorBase plus { get; }
        public abstract FluentCalculatorBase minus { get; }
        public abstract FluentCalculatorBase times { get; }
        public abstract FluentCalculatorBase devidedBy { get; }
        
        /// <summary>
        /// Метод, определяющий логику преобразования результата вычислений к типу int.
        /// Должен быть реализован в классе-наследнике.
        /// </summary>
        /// <returns>Значение результата вычислений, приведенное к integer.</returns>
        private protected abstract int ConvertToInt();

        /// <summary>
        /// Неявное преобразование экземпляра калькулятора к типу integer.
        /// </summary>
        /// <param name="x"></param>
        public static implicit operator int(FluentCalculatorBase x)
        {
            return x.ConvertToInt();
        }
    }
}