﻿namespace Calculator
{
    /// <summary>
    /// Вариант реализации калькулятора без учета приоритета операций (выполнение в порядке вызова).
    /// </summary>
    public class FluentCalculator : FluentCalculatorBase
    {
        private bool _isValue = false;

        private Operation? _chosenOperation = null;

        public override FluentCalculator plus
        {
            get
            {
                if (_isValue)
                {
                    _chosenOperation = Operation.Plus;
                    _isValue = false;

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Operation should be used after a value.");
                }
            }
        }

        public override FluentCalculator minus
        {
            get
            {
                if (_isValue)
                {
                    _chosenOperation = Operation.Minus;
                    _isValue = false;

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Operation should be used after a value.");
                }
            }
        }

        public override FluentCalculator times
        {
            get
            {
                if (_isValue)
                {
                    _chosenOperation = Operation.Times;
                    _isValue = false;

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Operation should be used after a value.");
                }
            }
        }

        public override FluentCalculator devidedBy
        {
            get
            {
                if (_isValue)
                {
                    _chosenOperation = Operation.DividedBy;
                    _isValue = false;

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Operation should be used after a value.");
                }
            }
        }

        public override FluentCalculator one
        {
            get
            {
                if (!_isValue)
                {
                    _isValue = true;
                    Calculate(1);

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Value should be used after an operation or as start of a chain.");
                }
            }
        }

        public override FluentCalculator two
        {
            get
            {
                if (!_isValue)
                {
                    _isValue = true;
                    Calculate(2);

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Value should be used after an operation or as start of a chain.");
                }
            }
        }

        public override FluentCalculator three
        {
            get
            {
                if (!_isValue)
                {
                    _isValue = true;
                    Calculate(3);

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Value should be used after an operation or as start of a chain.");
                }
            }
        }

        public override FluentCalculator four
        {
            get
            {
                if (!_isValue)
                {
                    _isValue = true;
                    Calculate(4);

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Value should be used after an operation or as start of a chain.");
                }
            }
        }

        public override FluentCalculator five
        {
            get
            {
                if (!_isValue)
                {
                    _isValue = true;
                    Calculate(5);

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Value should be used after an operation or as start of a chain.");
                }
            }
        }

        public override FluentCalculator six
        {
            get
            {
                if (!_isValue)
                {
                    _isValue = true;
                    Calculate(6);

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Value should be used after an operation or as start of a chain.");
                }
            }
        }

        public override FluentCalculator seven
        {
            get
            {
                if (!_isValue)
                {
                    _isValue = true;
                    Calculate(7);

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Value should be used after an operation or as start of a chain.");
                }
            }
        }

        public override FluentCalculator eight
        {
            get
            {
                if (!_isValue)
                {
                    _isValue = true;
                    Calculate(8);

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Value should be used after an operation or as start of a chain.");
                }
            }
        }

        public override FluentCalculator nine
        {
            get
            {
                if (!_isValue)
                {
                    _isValue = true;
                    Calculate(9);

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Value should be used after an operation or as start of a chain.");
                }
            }
        }

        public override FluentCalculator ten
        {
            get
            {
                if (!_isValue)
                {
                    _isValue = true;
                    Calculate(10);

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Value should be used after an operation or as start of a chain.");
                }
            }
        }

        private protected override int ConvertToInt()
        {
            return (int)Math.Ceiling(_value);
        }

        private void Calculate(float operand)
        {
            switch (_chosenOperation)
            {
                case Operation.Plus:
                    _value += operand;
                    break;
                case Operation.Minus:
                    _value -= operand;
                    break;
                case Operation.Times:
                    _value *= operand;
                    break;
                case Operation.DividedBy:
                    _value /= operand;
                    break;
                default:
                    _value = operand;
                    break;
            }
        }

        private enum Operation
        {
            Plus,
            Minus,
            Times,
            DividedBy
        }
    }
}