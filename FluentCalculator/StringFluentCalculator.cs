﻿using System.Data;
using System.Text;

namespace Calculator
{
    /// <summary>
    /// Вариант реализации калькулятора с учетом приоритета операций (вычисление на основе System.DataTable.Compute).
    /// </summary>
    public class StringFluentCalculator : FluentCalculatorBase
    {
        private bool _isValue = false;
        private readonly StringBuilder expression;

        public StringFluentCalculator()
        {
            expression = new StringBuilder();
        }

        public override StringFluentCalculator plus
        {
            get
            {
                if (_isValue)
                {
                    expression.Append((char)Operation.Plus);
                    _isValue = false;

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Operation should be used after a value.");
                }
            }
        }

        public override StringFluentCalculator minus
        {
            get
            {
                if (_isValue)
                {
                    expression.Append((char)Operation.Minus);
                    _isValue = false;

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Operation should be used after a value.");
                }
            }
        }

        public override StringFluentCalculator times
        {
            get
            {
                if (_isValue)
                {
                    expression.Append((char)Operation.Times);
                    _isValue = false;

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Operation should be used after a value.");
                }
            }
        }

        public override StringFluentCalculator devidedBy
        {
            get
            {
                if (_isValue)
                {
                    expression.Append((char)Operation.DividedBy);
                    _isValue = false;

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Operation should be used after a value.");
                }
            }
        }

        public override StringFluentCalculator one
        {
            get
            {
                if (!_isValue)
                {
                    _isValue = true;
                    expression.Append(1);

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Value should be used after an operation or as start of a chain.");
                }
            }
        }

        public override StringFluentCalculator two
        {
            get
            {
                if (!_isValue)
                {
                    _isValue = true;
                    expression.Append(2);

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Value should be used after an operation or as start of a chain.");
                }
            }
        }

        public override StringFluentCalculator three
        {
            get
            {
                if (!_isValue)
                {
                    _isValue = true;
                    expression.Append(3);

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Value should be used after an operation or as start of a chain.");
                }
            }
        }

        public override StringFluentCalculator four
        {
            get
            {
                if (!_isValue)
                {
                    _isValue = true;
                    expression.Append(4);

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Value should be used after an operation or as start of a chain.");
                }
            }
        }

        public override StringFluentCalculator five
        {
            get
            {
                if (!_isValue)
                {
                    _isValue = true;
                    expression.Append(5);

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Value should be used after an operation or as start of a chain.");
                }
            }
        }

        public override StringFluentCalculator six
        {
            get
            {
                if (!_isValue)
                {
                    _isValue = true;
                    expression.Append(6);

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Value should be used after an operation or as start of a chain.");
                }
            }
        }

        public override StringFluentCalculator seven
        {
            get
            {
                if (!_isValue)
                {
                    _isValue = true;
                    expression.Append(7);

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Value should be used after an operation or as start of a chain.");
                }
            }
        }

        public override StringFluentCalculator eight
        {
            get
            {
                if (!_isValue)
                {
                    _isValue = true;
                    expression.Append(8);

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Value should be used after an operation or as start of a chain.");
                }
            }
        }

        public override StringFluentCalculator nine
        {
            get
            {
                if (!_isValue)
                {
                    _isValue = true;
                    expression.Append(9);

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Value should be used after an operation or as start of a chain.");
                }
            }
        }

        public override StringFluentCalculator ten
        {
            get
            {
                if (!_isValue)
                {
                    _isValue = true;
                    expression.Append(10);

                    return this;
                }
                else
                {
                    throw new InvalidOperationException("Value should be used after an operation or as start of a chain.");
                }
            }
        }

        private protected override int ConvertToInt()
        {
            var result = (int)Math.Ceiling((double)new DataTable().Compute(expression.ToString(), null));
            return result;
        }

        private enum Operation
        {
            Plus = '+',
            Minus = '-',
            Times = '*',
            DividedBy = '/'
        }
    }
}