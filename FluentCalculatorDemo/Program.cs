﻿using Calculator;
using System.Text;

Console.OutputEncoding = Encoding.UTF8;
FluentCalculatorBase calc = new FluentCalculator();
var test = calc.nine.devidedBy.three.plus.five.times.three - 10;
Console.WriteLine($"Результат вычисления 'FluentCalculator.nine.devidedBy.three.plus.five.times.three - 10': {test}");

calc = new StringFluentCalculator();
test = calc.nine.devidedBy.three.plus.five.times.three - 10;
Console.WriteLine($"Результат вычисления 'StringFluentCalculator.nine.devidedBy.three.plus.five.times.three - 10': {test}");

//Вариант решения задачи No.1.
BrokenBool brokenBool = new BrokenBool();
var result = brokenBool == true && brokenBool == false;
Console.WriteLine($"Результат выполнения 'brokenBool == true && brokenBool == false': {result}");

public class BrokenBool
{
    private bool _boolean = false;

    public static implicit operator bool(BrokenBool x)
    {
        return x._boolean = !x._boolean;
    }
}